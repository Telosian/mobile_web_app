const http = require('http');
const server = http.createServer();
server.listen(8080);  // On écoute sur le port 8080

// Création du server WebSocket qui utilise le serveur précédent
const WebSocketServer = require('websocket').server;
const wss = new WebSocketServer({
    httpServer: server
});


// Mise en place des événements WebSockets
wss.on('request', function(request) {
    const connection = request.accept(null, request.origin);

    connection.on('message', function(data) {
        console.log(data);
        data=JSON.parse(data.utf8Data)
        if(data.key==="dedans"){
            if(data.pos.x<50||data.pos.x>950||data.pos.y<50||data.pos.y>950){
                connection.send(false)
            }
            connection.send(true)
        }
        connection.send("feedback");

    });



    // Fermeture de la connection après avoir log la raison de la déconnexion
    connection.on('close', function(reasonCode, description) {
        console.log(reasonCode + description);
        connection.close();
    });
});
