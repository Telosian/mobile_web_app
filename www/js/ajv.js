const Ajv = require('ajv')

const ajv = new Ajv();
const draft6MetaSchema = require("ajv/dist/refs/json-schema-draft-06.json");
ajv.addMetaSchema(draft6MetaSchema);

let validate_user = ajv.compile(require("../schema/user.json"));

const validPlayer = {
    id: 1,
    username: "test",
    password: "test"
}

const invalidPlayer = {
    id: 1,
    username: "test"
}

console.log(validate_user(validPlayer));
console.log(validate_user(invalidPlayer));
