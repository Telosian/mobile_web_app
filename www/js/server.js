const http = require('http');
const server = http.createServer();
server.listen(8080);  // On écoute sur le port 8080

let mongoose = require('mongoose');

const {PlayerSchema} = require("../node_modules/models/Player");
const {PartySchema} = require("../node_modules/models/Party");
const {PieceSchema} = require("../node_modules/models/Pion");
const {PositionSchema} = require("../node_modules/models/Position");

main().catch(err => console.log(err));

async function main() {
    await mongoose.connect("mongodb://127.0.0.1:27017/mongodoc");
    const Player = mongoose.model('Player', PlayerSchema);
    const Party = mongoose.model('Party', PartySchema);
    const Piece = mongoose.model('Piece', PieceSchema);
    const Position = mongoose.model('Position', PositionSchema);

    // Création du server WebSocket qui utilise le serveur précédent
    const WebSocketServer = require('websocket').server;
    const wss = new WebSocketServer({
        httpServer: server
    });

    let queue = [];

    wss.on('request', function (request) {
        const connection = request.accept(null, request.origin);

        connection.on("message", function (data) {
            let receivedData = JSON.parse(data.utf8Data);

            switch (receivedData.key) {
                case "register":
                    let registerData = receivedData.data;
                    (async () => {
                        try {
                            let id = await Player.find().sort("-id").limit(1).select('id');
                            id = id.length === 0 ? 0 : id[id.length-1].id + 1;

                            Player.findOne({username: registerData.username}, (err, user) => {
                                if (user) connection.send(JSON.stringify({valid: false, message: 1}));
                            })

                            let newPlayer = new Player({
                                id: id,
                                username: registerData.username,
                                password: registerData.password,
                                parties: []
                            });

                            await newPlayer.save().then(() => connection.send(JSON.stringify({valid: true, message: "inserted"})));
                        } catch (e) {
                            console.log(e);
                        }
                    })();
                    break;
                case "login":
                    let loginData = receivedData.data;
                    (async () => {
                        try {
                            Player.findOne({username: loginData.username}, (err, user) => {
                                if (user) {
                                    if (user.password === loginData.password) connection.send(JSON.stringify({valid: true, message: user.id}))
                                } else {
                                    connection.send(JSON.stringify({valid: false, message: 0}));
                                }
                            })
                        } catch (e) {
                            console.log(e);
                        }
                    })();
                    break;
                case "joinParty":
                    if (queue.length <= 2) {
                        queue.push({playerId: receivedData.playerId, connection});
                    }

                    if (queue.length === 2) {
                        (async () => {
                            try {
                                let lastPartyId = await Party.find().sort("-id").limit(1).select('id');
                                let id = lastPartyId.length === 0 ? 0 : lastPartyId[lastPartyId.length-1].id + 1;
                                let newParty = new Party({
                                    id: parseInt(id)+1,
                                    id_player_1: queue[0].playerId,
                                    id_player_2: queue[1].playerId,
                                    who_play: queue[0].playerId,
                                    winner: null,
                                })

                                newParty.save();

                                for (let element of queue) element.connection.send(JSON.stringify({valid: true, message: newParty}));
                                console.log(newParty);
                            } catch (e) {
                                console.log(e);
                            }
                        })();
                    }
                    break;
                case "initParty":
                    (async () => {
                        try {
                            Party.findOne({id: receivedData.partyId}, async (err, party) => {
                                for (let piece of receivedData.player1.pieces) {
                                    let newPos = new Position( {
                                        id: piece.id,
                                        x: piece.x,
                                        y: piece.y,
                                        party_id: receivedData.partyId,
                                    });

                                    let newPiece = new Piece({
                                        id: id,
                                        color: piece.color,
                                        is_queen: false,
                                        position_id: id,
                                        player_id: receivedData.player1.id,
                                        party_id: receivedData.partyId
                                    });
                                    await newPiece.save().then(() => console.log("Nouveau pion saved"))
                                }
                                for (let piece of receivedData.player2.pieces) {
                                    let newPos = new Position( {
                                        id: piece.id,
                                        x: piece.x,
                                        y: piece.y,
                                        party_id: receivedData.partyId,
                                    });

                                    let newPiece = new Piece({
                                        id: id,
                                        color: piece.color,
                                        is_queen: false,
                                        position_id: id,
                                        player_id: receivedData.player1.id,
                                        party_id: receivedData.partyId
                                    });
                                    await newPiece.save().then(() => console.log("Nouveau pion saved"))
                                }
                            })
                            for (let element of queue) {
                                element.connection.send(JSON.stringify({
                                    valid: true,
                                    message:
                                        element.playerId === receivedData.player1.id
                                            ? receivedData.player1
                                            : receivedData.player2
                                }));
                            }
                        } catch (e) {
                            console.log(e);
                        }
                    })();
                    break;
                default:
                    break;
            }
        });

        connection.on('close', function (reasonCode, description) {
            console.log("Code : " + reasonCode + ", Reason : " + description);
            connection.close();
        });
    });
}
