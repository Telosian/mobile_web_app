let errorMessages = null;
let formErrorRegister = null;
let playerId = null;
let partyId = null;

function changeLocation(location) {
    if (playerId === null && (["home","board"].includes(location)))  {
        let errorDiv = document.getElementById("bypassError");
        errorDiv.style.display = "block";
        errorDiv.textContent = "C'est pas bien d'essayer de tricher !";
    } else {
        let pages = document.querySelectorAll(".page");
        for (let page of pages) {
            if (page.id === location) {
                page.classList.remove("d-none");
            } else {
                page.classList.add("d-none");
            }
        }
    }
}

/****** Board ******/
const boardSize = 1000;
const rowCellCount = 10;
const piecePerPlayer = 20;

const cellSize = boardSize / rowCellCount;
const pieceSize = 0.8 * (cellSize / 2);
const pieceStrokeSize = 0.8 * (cellSize / 2) / 10;

const spaceBetweenPieceOnRow = 2 * cellSize;
const spaceBetweenPieceOnCol = cellSize;

const startPosEven = cellSize / 2;
const startPosOdd = startPosEven + cellSize;
const endPosEven = boardSize - startPosEven;
const endPosOdd = endPosEven - cellSize;

let selectedPiece;
let whoPlay = 1;
let currentPlayer = 1;
let clickableCases = [];

const svg = document.getElementById("board-svg");
svg.setAttributeNS(null, "viewBox", "0 0 " + boardSize + " " + boardSize);

function initPiecesPlace(player, party) {
    let x = player.id === party.id_player_1 ? startPosOdd : endPosOdd;
    let y = player.id === party.id_player_1 ? startPosEven : endPosEven;

    player.pieces.forEach(piece => {
        piece.x = x;
        piece.y = y;
        x += player.id === party.id_player_1 ? spaceBetweenPieceOnRow : -spaceBetweenPieceOnRow;

        if ((x >= endPosEven + spaceBetweenPieceOnRow || x === startPosEven - spaceBetweenPieceOnRow) &&
            (y / (cellSize / 2) % 2 === 1 || y === (cellSize / 2))) {
            y += player.id === party.id_player_1 ? spaceBetweenPieceOnCol : -spaceBetweenPieceOnCol;
            x = player.id === party.id_player_1 ? startPosEven : endPosEven;
        } else if ((x >= endPosOdd + spaceBetweenPieceOnRow || x === startPosOdd - spaceBetweenPieceOnRow) &&
            (y / (cellSize / 2)) % 2 === 1) {
            y += player.id === party.id_player_1 ? spaceBetweenPieceOnCol : -spaceBetweenPieceOnCol;
            x = player.id === party.id_player_1 ? startPosOdd : endPosOdd;
        }
    })
}


function createSVGPieces(player, party) {
    let options = player.id === party.id_player_1
        ? {
            fill: 'rgba(20,20,20,1)',
            stroke: 'white',
        }
        : {
            fill: 'rgba(250,250,250,1)',
            stroke: 'black',
        }
    let pieceID;
    player.pieces.forEach(piece => {
        pieceID = piece.id;
        let baseOpt = {
            id: piece.id,
            cx: piece.x,
            cy: piece.y,
            r: pieceSize,
            'stroke-width': pieceStrokeSize,
        }
        let svgElement = getSvgElement('circle', {...baseOpt, ...options})
        svgElement.addEventListener("click", function () {
            selectPiece(pieceID)
        })
        svg.appendChild(svgElement);
    })
}

function getSvgElement(svgTag, options) {
    let element = document.createElementNS("http://www.w3.org/2000/svg", svgTag);
    for (let option in options) { //parcour des clés des l'objets options
        element.setAttributeNS(null, option, options[option]);
    }
    return element;
}

function initBoard() {
    let idColor, x, y = 0;
    let color = '';
    let count = 1;
    for (let row = 0; row < rowCellCount; row++) {
        x = 0
        idColor = row % 2 === 1 ? 0 : 1;
        for (let col = 0; col < rowCellCount; col++) {
            color = idColor % 2 === 1 ? "#E0CDA9" : "#582900";
            let svgElement = getSvgElement('rect', {x: x, y: y, width: cellSize, height: cellSize, fill: color});
            if (color === "#582900") {
                svgElement.dataset.id = "c" + count;
                count++;
            }
            svg.appendChild(svgElement);
            x += cellSize;
            idColor += 1;
        }
        y += cellSize;
    }
}

/************ Nicolas ************/

let pieceTrial = JSON.stringify({
    pieces: [
        {id: 0, id_Joueur: 0, couleur: 0, is_queen: false, x: 50, y: 550},
        {id: 1, id_Joueur: 0, couleur: 0, is_queen: false, x: 250, y: 550},
        {id: 2, id_Joueur: 1, couleur: 1, is_queen: false, x: 450, y: 550}
    ]
})
let casesTrial = JSON.stringify({
    cases: [
        {x: 300, y: 400},
        {x: 500, y: 400}
    ]
})

function initPieces(playerId, ordre) {
    let pieces = [];
    if (ordre === 1) {
        for (let count = 0; count < piecePerPlayer; count++) {
            pieces.push({
                id: count,
                playerId: playerId,
                color: 0,
                x: 0,
                y: 0,
                isQueen: false
            })
        }
    } else if (ordre === 2) {
        for (let count = piecePerPlayer; count < 2*piecePerPlayer; count++) {
            pieces.push({
                id: count,
                playerId: playerId,
                color: 1,
                x: 0,
                y: 0,
                isQueen: false
            })
        }
    }
    return pieces;
}

let player1 = {
    id: 0,
    username: 'test1',
    password: 'test1',
}

let player2 = {
    id: 1,
    username: 'test2',
    password: 'test2',
}

function refresh(player1, player2, Jsontest) {
    player1.pieces = [];
    player2.pieces = [];
    console.log("Json avant " + Jsontest);
    Jsontest = JSON.parse(Jsontest);
    console.log(Jsontest.Pion)
    Jsontest = Jsontest.Pion;
    console.log("Json après " + Jsontest);
    for (let Pion of Jsontest) {
        console.log("pion: " + Pion);
        console.log("pion.id: " + Pion.id);
        if (Pion.id_Joueur === player1.id) {
            player1.pieces.push({
                id: Pion.id,
                playerId: Pion.id_Joueur,
                color: Pion.couleur,
                x: Pion.x,
                y: Pion.y,
                isQueen: Pion.is_queen
            })
        } else if (Pion.id_Joueur === player2.id) {
            player2.pieces.push({
                id: Pion.id,
                playerId: Pion.id_Joueur,
                color: Pion.couleur,
                x: Pion.x,
                y: Pion.y,
                isQueen: Pion.is_queen
            })
        } else {
            console.log("Erreur au pion :" + Pion.id + " Joueur: " + Pion.id_Joueur + " non trouvée");
        }
    }
    createSVGPieces(player1);
    createSVGPieces(player2);
}

function movePiece(pieceId, x, y) {
    document.getElementById(pieceId).remove();
    let baseOpt = {
        id: pieceId,
        cx: x,
        cy: y,
        r: pieceSize,
        'stroke-width': pieceStrokeSize,
    }
    let options = pieceId < 20
        ? {
            fill: 'rgba(20,20,20,1)',
            stroke: 'white',
        }
        : {
            fill: 'rgba(250,250,250,1)',
            stroke: 'black',
        }
    let svgElement = getSvgElement('circle', {...baseOpt, ...options})
    svg.appendChild(svgElement);
}

// pion et case cliquable
function selectPiece(pieceId) {
    console.log("Pièce id : ", pieceId);
    if (currentPlayer === 1 && pieceId < 20 || currentPlayer === 1 && pieceId >= 20) {
        selectedPiece = pieceId;
        let pion = document.getElementById(pieceId)
        removeClickOnCase();
        addClickOnCase(casesTrial);
        //ToDO function appel le serveur qui nous renvoient les cases valides;
    }
}


// le JsonCase doit resembler a ceci {Case: {{"x":"300","y":"400"},{"x":"500","y":"400"}}
function addClickOnCase(cases) {
    console.log(JSON.parse(cases))
    cases = JSON.parse(cases);
    clickableCases = cases;

    for (let Case of clickableCases.cases) {
        let x = Case.x;
        let mx = x + 50;
        let y = Case.y;
        let my = y + 50;
        document
            .querySelector("rect[x='" + x + "'][y='" + y + "']")
            .setAttribute("onclick", "movePiece(" + selectedPiece + "," + mx + "," + my + ");");
    }

}

function removeClickOnCase() {
    if (clickableCases.length === 0) return;
    for (let Case of clickableCases.cases) {
        let x = Case.x;
        let y = Case.y;
        document
            .querySelector("rect[x='" + x + "'][y='" + y + "']")
            .removeAttribute("onclick");
    }
    clickableCases = [];
}

function yourTurn() {
    if (whoPlay === currentPlayer) {
        for (let i = 0; i < piecePerPlayer; i++) {
            if (document.getElementById("" + i) !== null) {
                console.log("pion : " + document.getElementById("" + i));
                document.getElementById("" + i).setAttribute("onclick", "selectPiece(" + i + ")");
            }
        }
    } else if (whoPlay !== currentPlayer) {
        for (let i = 0; i < piecePerPlayer; i++) {
            if (document.getElementById("" + i) !== null) {
                console.log("pion : " + document.getElementById("" + i));
                document.getElementById("" + i).removeAttribute("onclick");
            }
        }
    } else if (whoPlay !== currentPlayer) {
        for (let i = piecePerPlayer; i < 2 * piecePerPlayer; i++) {
            if (document.getElementById("" + i) !== null) {
                console.log("pion : " + document.getElementById("" + i));
                document.getElementById("" + i).removeAttribute("onclick");
            }
        }
    } else if (whoPlay === currentPlayer) {
        for (let i = piecePerPlayer; i < 2 * piecePerPlayer; i++) {
            if (document.getElementById("" + i) !== null) {
                console.log("pion : " + document.getElementById("" + i));
                document.getElementById("" + i).setAttribute("onclick", "selectPiece(" + i + ")");
            }
        }
    }

}

document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    // Cordova is now initialized. Have fun!

    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);

    let errorMessages = null;
    let formErrorRegister = null;

    let ws = null;

    // let platform = device.platform;
    // if (platform === "browser") ws = new WebSocket('ws://127.0.0.1:8080/');
    // else ws = new WebSocket('ws://10.0.2.2:8080/');

    ws = new WebSocket('ws://127.0.0.1:8080/')

    ws.onopen = () => {
        console.log('Connexion effective');
    };

    /****** Register ******/
    document.getElementById("register").addEventListener("submit", (e) => {
        e.preventDefault();

        let formElements = e.target.elements;

        let username = formElements.pseudo.value === formElements.pseudoC.value ? formElements.pseudo.value : null;
        let password = formElements.password.value === formElements.passwordC.value ? formElements.password.value : null;

        if (username === null || password === null) {
            formErrorRegister = 0;
        } else {
            ws.send(JSON.stringify({key:'register', data:{username, password}}));
            ws.onmessage = function (response) {
                if (!JSON.parse(response.data).valid) formErrorRegister = JSON.parse(response.data).message;
                else changeLocation('login');
            };
        }

        errorMessages = document.getElementById('formErrorRegister');
        if (formErrorRegister === 0) {
            errorMessages.style.display = "block";
            errorMessages.textContent = "Un champ de confirmation n'est pas renseigné correctement.";
        }
        if (formErrorRegister === 1) {
            errorMessages.style.display = "block";
            errorMessages.textContent = "Le pseudo est déjà pris veuillez en renseigner un différent.";
        }
    })


    /****** Login ******/
    document.getElementById("login").addEventListener("submit", (e) => {
        e.preventDefault();

        let formElements = e.target.elements;

        ws.send(JSON.stringify({
            key:'login',
            data:{
                username: formElements.pseudo.value,
                password: formElements.password.value
            }
        }));
        ws.onmessage = function (response) {
            if (!JSON.parse(response.data).valid)
                errorMessages = document.getElementById('formErrorLogin');
            if (JSON.parse(response.data).message === 0) {
                errorMessages.style.display = "block";
                errorMessages.textContent = "Identifiants non reconnus. Veuillez réessayer.";
            }
            else {
                playerId = JSON.parse(response.data).message;
                changeLocation('home');
            }
        };
    })

    /****** Party ******/
    document.getElementById("launchParty").addEventListener("click", (e) => {
        changeLocation("board");

        let waiting = document.getElementById("waiting");
        let board = document.getElementById("board-svg");
        ws.send(JSON.stringify({key:'joinParty', playerId}));
        ws.onmessage = function (response) {
            if (JSON.parse(response.data).valid) {
                partyId = JSON.parse(response.data).message.id;
                waiting.classList.add("d-none");
                board.classList.remove("d-none");

                initBoard();

                console.log(JSON.parse(response.data).message.id_player_1);
                player1.id = JSON.parse(response.data).message.id_player_1;
                player2.id = JSON.parse(response.data).message.id_player_2;

                player1.pieces = initPieces(player1.id, JSON.parse(response.data).message.id_player_1 === JSON.parse(response.data).message.who_play ? 1 : 2)
                player2.pieces = initPieces(player2.id, JSON.parse(response.data).message.id_player_2 === JSON.parse(response.data).message.who_play ? 1 : 2)

                initPiecesPlace(player1, JSON.parse(response.data).message);
                initPiecesPlace(player2, JSON.parse(response.data).message);

                createSVGPieces(player1, JSON.parse(response.data).message);
                createSVGPieces(player2, JSON.parse(response.data).message);

                clickableCases = JSON.parse(casesTrial);
                yourTurn()
            }
            else changeLocation('login');
        };
    })

}
