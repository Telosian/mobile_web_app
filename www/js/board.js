const boardSize = 1000;
const rowCellCount = 10;
const piecePerPlayer = 20;

const cellSize = boardSize / rowCellCount;
const pieceSize = 0.8 * (cellSize / 2);
const pieceStrokeSize = 0.8 * (cellSize / 2) / 10;

const spaceBetweenPieceOnRow = 2 * cellSize;
const spaceBetweenPieceOnCol = cellSize;

const startPosEven = cellSize / 2;
const startPosOdd = startPosEven + cellSize;
const endPosEven = boardSize - startPosEven;
const endPosOdd = endPosEven - cellSize;
let IdpionSelect;
let tourde = 1;
let vous;
let CaseClickable = [];

const svg = document.getElementById("board-svg");
svg.setAttributeNS(null, "viewBox", "0 0 " + boardSize + " " + boardSize);

function initPieces(playerId) {
    let pieces = [];
    for (let count = 0; count < piecePerPlayer; count++) {
        pieces.push({
            id: count,
            playerId: playerId,
            color: playerId,
            x: 0,
            y: 0,
            isQueen: false
        })
    }
    return pieces;
}


function initPieces2(playerId, ordre) {
    let pieces = [];
    if (ordre === 1) {
        for (let count = 0; count < piecePerPlayer; count++) {
            pieces.push({
                id: count,
                playerId: playerId,
                color: 0,
                x: 0,
                y: 0,
                isQueen: false
            })
        }
    } else if (ordre === 2) {
        for (let count = piecePerPlayer; count < 2 * piecePerPlayer; count++) {
            pieces.push({
                id: count,
                playerId: playerId,
                color: 1,
                x: 0,
                y: 0,
                isQueen: false
            })
        }
    }
    return pieces;
}


// pion et case cliquable
function selectPion(idpion) {
    if (vous === 1 && idpion < 20 || vous === 1 && idpion >= 20) {
        IdpionSelect = idpion;
        let pion = document.getElementById(idpion)
        //ToDO function qui rend les case jouable pour ce pion clicable avec move(idpion,coordonéX,coordonéY) et posssiblement remove les case qui était clicable avant (changement de pion séléctionner)
    }
}


// le JsonCase doit resembler a ceci {{"x":"300","y":"400"},{"x":"500","y":"400"}}
function AddCaseOnclick(JsonCase) {
    let CasePossible = JSON.parse(JsonCase);
    CaseClickable.push(CasePossible);
    for(let Case of CasePossible.Case){
        let x=Case.x;
        let y=Case.y;
        let mx=x+50;
        let my=y+50;
        console.log("mx="+mx);
        console.log("my="+my);
        document.querySelector("rect[x='" + x + "'][y='" + y + "']").setAttribute("onclick", "movePion(" + IdpionSelect + "," + mx + "," + my + ");");
    }

}

function removeCaseOnclick() {
    for (let Case of CaseClickable) {
        let x = Case.x - 50;
        let y = Case.y - 50;
        document.querySelector("rect[x='" + x + "'][y='" + y + "']").removeAttribute("onclick");
    }
    CaseClickable = [];
}

function chgTour() {
    tourde = tourde === 1 ? 2 : 1;
}

function setVous(ordre) {
    vous = ordre;
}


function initPiecesPlace(player) {
    let x = player.id === 0 ? startPosOdd : endPosOdd;
    let y = player.id === 0 ? startPosEven : endPosEven;

    player.pieces.forEach(piece => {
        piece.x = x;
        piece.y = y;
        x += player.id === 0 ? spaceBetweenPieceOnRow : -spaceBetweenPieceOnRow;

        if ((x >= endPosEven + spaceBetweenPieceOnRow || x === startPosEven - spaceBetweenPieceOnRow) &&
            (y / (cellSize / 2) % 2 === 1 || y === (cellSize / 2))) {
            y += player.id === 0 ? spaceBetweenPieceOnCol : -spaceBetweenPieceOnCol;
            x = player.id === 0 ? startPosEven : endPosEven;
        } else if ((x >= endPosOdd + spaceBetweenPieceOnRow || x === startPosOdd - spaceBetweenPieceOnRow) &&
            (y / (cellSize / 2)) % 2 === 1) {
            y += player.id === 0 ? spaceBetweenPieceOnCol : -spaceBetweenPieceOnCol;
            x = player.id === 0 ? startPosOdd : endPosOdd;
        }
    })
}

function movePion(idpion, x, y) {
    document.getElementById(idpion).remove();
    let baseOpt = {
        id: idpion,
        cx: x,
        cy: y,
        r: pieceSize,
        'stroke-width': pieceStrokeSize,
    }
    let options = idpion < 20
        ? {
            fill: 'rgba(20,20,20,1)',
            stroke: 'white',
        }
        : {
            fill: 'rgba(250,250,250,1)',
            stroke: 'black',
        }
    let svgElement = getSvgElement('circle', {...baseOpt, ...options})
    svg.appendChild(svgElement);
}


function createSVGPieces(player) {
    let options = player.id === 0
        ? {
            fill: 'rgba(20,20,20,1)',
            stroke: 'white',
        }
        : {
            fill: 'rgba(250,250,250,1)',
            stroke: 'black',
        }
    let pieceID;
    player.pieces.forEach(piece => {
        pieceID = piece.id;
        let baseOpt = {
            id: piece.id,
            cx: piece.x,
            cy: piece.y,
            r: pieceSize,
            'stroke-width': pieceStrokeSize,
        }
        let svgElement = getSvgElement('circle', {...baseOpt, ...options})
        svgElement.addEventListener("click", function () {
            selectPion(pieceID)
        })
        svg.appendChild(svgElement);
    })
}

function getSvgElement(svgTag, options) {
    let element = document.createElementNS("http://www.w3.org/2000/svg", svgTag);
    for (let option in options) { //parcour des clés des l'objets options
        element.setAttributeNS(null, option, options[option]);
    }
    return element;
}

let player1 = {
    id: 0,
    ordre: 1,
    username: 'test1',
    password: 'test1',
    pieces: initPieces2(0, 1),
}

let player2 = {
    id: 1,
    ordre: 2,
    username: 'test2',
    password: 'test2',
    pieces: initPieces2(0, 2),
}

function initBoard() {
    let idColor, x, y = 0;
    let color = '';
    let count = 1;
    for (let row = 0; row < rowCellCount; row++) {
        x = 0
        idColor = row % 2 === 1 ? 0 : 1;
        for (let col = 0; col < rowCellCount; col++) {
            color = idColor % 2 === 1 ? "#E0CDA9" : "#582900";
            let svgElement = getSvgElement('rect', {x: x, y: y, width: cellSize, height: cellSize, fill: color});
            if (color === "#582900") {
                svgElement.dataset.id = "c" + count;
                count++;
            }
            svg.appendChild(svgElement);
            x += cellSize;
            idColor += 1;
        }
        y += cellSize;
    }
}

let Jsontest = JSON.stringify({
    Pion: [{id: 0, id_Joueur: 0, couleur: 0, is_queen: false, x: 50, y: 550},
        {id: 1, id_Joueur: 0, couleur: 0, is_queen: false, x: 250, y: 550},
        {id: 2, id_Joueur: 1, couleur: 1, is_queen: false, x: 450, y: 550}]
})
let JsonTestCase = JSON.stringify({
    Case: [{x: 300, y: 400},
        {x: 500, y: 400}]})


function Actualise(player1, player2, Jsontest) {
    player1.pieces = [];
    player2.pieces = [];
    console.log("Json avant " + Jsontest);
    Jsontest = JSON.parse(Jsontest);
    console.log(Jsontest.Pion)
    Jsontest = Jsontest.Pion;
    console.log("Json après " + Jsontest);
    for (let Pion of Jsontest) {
        console.log("pion: " + Pion);
        console.log("pion.id: " + Pion.id);
        if (Pion.id_Joueur === player1.id) {
            player1.pieces.push({
                id: Pion.id,
                playerId: Pion.id_Joueur,
                color: Pion.couleur,
                x: Pion.x,
                y: Pion.y,
                isQueen: Pion.is_queen
            })
        } else if (Pion.id_Joueur === player2.id) {
            player2.pieces.push({
                id: Pion.id,
                playerId: Pion.id_Joueur,
                color: Pion.couleur,
                x: Pion.x,
                y: Pion.y,
                isQueen: Pion.is_queen
            })
        } else {
            console.log("Erreur au pion :" + Pion.id + " Joueur: " + Pion.id_Joueur + " non trouvée");
        }
    }
    createSVGPieces(player1);
    createSVGPieces(player2);
}


initBoard();

initPiecesPlace(player1);
createSVGPieces(player1);

initPiecesPlace(player2);
createSVGPieces(player2);
movePion(15, 150, 450);
//console.log(document.querySelector("rect[x='" + 0 + "'][y='" + 500 + "']").getAttribute('data-id'));
//Actualise(player1, player2, Jsontest);
//console.log(Math.ceil((parseInt("c11"))/5%2)-1);

IdpionSelect=17;
AddCaseOnclick(JsonTestCase);
